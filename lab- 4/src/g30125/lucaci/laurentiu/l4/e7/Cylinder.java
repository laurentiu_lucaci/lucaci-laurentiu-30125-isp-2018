package g30125.lucaci.laurentiu.l4.e7;

import g30125.lucaci.laurentiu.l4.e3.Circle;

public class Cylinder extends Circle{
	double height;
	public Cylinder(double radius)
	{
		super(radius);
	}
	public Cylinder()
	{
		height=1.0;
	}
	public Cylinder(double radius, double height)
	{
		height=1.0;
		radius=1.0;
	}
	double getHeight()
	{
		return height;
	}
	public double getVolume(double radius,double height)
	{
		return Math.PI*Math.pow(radius, 2)*height;
	}
	public double getArea(double radius)
	{
		return Math.PI*Math.pow(radius, 2);
	}
	public double getArea2(double radius, double height)
	{
		return 2*Math.PI*radius*(radius+height);
	}
}
