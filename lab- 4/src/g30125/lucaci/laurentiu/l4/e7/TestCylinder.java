package g30125.lucaci.laurentiu.l4.e7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCylinder {
	@Test
	public void shouldGetArea()
	{
		Cylinder c1=new Cylinder(2,5);
		assertEquals(87.9645943005, c1.getArea2(2,5),001);
	}
	@Test
	public void shouldGetVolume()
	{
		Cylinder c1=new Cylinder(2,5);
		assertEquals(62.8318530718, c1.getVolume(2,5),001);
	}
}
