package g30125.lucaci.laurentiu.l4.e3;

public class Circle {
	public double radius;
	String color;
	public Circle()
	{
		radius=1.0;
		color="red";
	}
	public Circle(double radius)
	{
		this.radius=radius;
		color="red";
	}
	public double getRadius()
	{
		return radius;
	}
	public double getArea()
	{
		return Math.PI*Math.pow(radius, 2);
	}
	
}
