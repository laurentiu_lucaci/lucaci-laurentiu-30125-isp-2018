package g30125.lucaci.laurentiu.l4.e9;

import becker.robots.*;

public class PoolDiving {
	public static void main(String[] args)
	{
		int i=0;
		City Cluj = new City();
	    Wall blockAve0 = new Wall(Cluj, 2, 2, Direction.EAST);
	    Wall blockAve1 = new Wall(Cluj, 3, 2, Direction.EAST);
	    Wall blockAve2 = new Wall(Cluj, 4, 2, Direction.EAST);
	    Wall blockAve3 = new Wall(Cluj, 4, 3, Direction.WEST);
	    Wall blockAve4 = new Wall(Cluj, 2, 3, Direction.NORTH);
	    Wall blockAve5 = new Wall(Cluj, 5, 3, Direction.NORTH);
	    Wall blockAve6 = new Wall(Cluj, 5, 4, Direction.NORTH);
	    Wall blockAve7 = new Wall(Cluj, 4, 4, Direction.EAST);
	    Robot mark = new Robot(Cluj, 1, 3, Direction.NORTH);
	    mark.move();
	    for(i=0;i<3;i++)
	    	mark.turnLeft();
	    mark.move();
	    for(i=0;i<3;i++)
	    	mark.turnLeft();
	    mark.move();
	    for(i=0;i<12;i++)
	    	mark.turnLeft();
	    mark.move();
	    mark.move();
	    mark.move();
	    for(i=0;i<2;i++)
	    	mark.turnLeft();
	    
	}
}
