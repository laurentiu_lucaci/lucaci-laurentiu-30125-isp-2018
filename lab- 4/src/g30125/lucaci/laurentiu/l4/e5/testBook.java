package g30125.lucaci.laurentiu.l4.e5;

import g30125.lucaci.laurentiu.l4.e4.Author;

public class testBook {
	public static void main(String[] args)
	{
		Author a1=new Author("Ion Creanga","blablbabla@nbalsda.com",'M');
		Book b1=new Book("Amintiri din copilarie",a1,200);
		Book b2=new Book("Luceafrul",new Author("Mihai Eminescu","blablbabla@nbalsda.com",'M'),15);
		Book b3=new Book("Balade",new Author("Tudor Arghezi","blablbabla@nbalsda.com",'M'),18);
		System.out.println(b1.tostring());
		System.out.println(b2.tostring());
		System.out.println(b3.tostring());
	}
}
