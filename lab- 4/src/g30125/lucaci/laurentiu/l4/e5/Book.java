package g30125.lucaci.laurentiu.l4.e5;

import g30125.lucaci.laurentiu.l4.e4.Author;

public class Book {
	String name;
	Author author;
	double price;
	int qtyInStock=0;
	public Book(String name, Author author, double price)
	{
		this.name=name;
		this.author=author;
		this.price=price;
	}
	public Book(String name, Author author, double price,int qtyInStock)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName()
	{
		return name;
	}
	public Author getAuthor()
	{
		return author;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price=price;
	}
	public int getQtyInStock()
	{
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	String tostring()
	{
		return name+" by "+getAuthor().tostring();
	}
}
