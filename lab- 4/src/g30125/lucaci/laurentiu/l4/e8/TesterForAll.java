package g30125.lucaci.laurentiu.l4.e8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TesterForAll {
	@Test
	public void shouldCircleGetArea()
	{
		Circle c1=new Circle(3,"Blue",true);
		assertEquals(28.274338823,c1.getArea(),001);
	}
	@Test
	public void shouldCircleGetPerimeter()
	{
		Circle c1=new Circle(3,"Blue",true);
		assertEquals(18.84955,c1.getPerimeter(),001);
	}
	@Test
	public void shouldRectangleGetArea()
	{
		Rectangle r1=new Rectangle(4,6,"Yellow",false);
		assertEquals(24,r1.getArea(),001);
	}
	@Test
	public void shouldRectangleGetPerimeter()
	{
		Rectangle r1=new Rectangle(4,6,"Yellow",false);
		assertEquals(20,r1.getPerimeter(),001);
	}
	
}
