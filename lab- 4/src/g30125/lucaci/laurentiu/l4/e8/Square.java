package g30125.lucaci.laurentiu.l4.e8;

import g30125.lucaci.laurentiu.l4.e8.Rectangle;
public class Square  extends Rectangle{
	public Square()
	{
		super();
	}
	public Square(double side)
	{
		super(side,side);
	}
	public Square(double side,String color, boolean filled)
	{
		super(side,side);
	}
	@Override
	public String tostring()
	{
		return "A Square with side "+super.getWidth()+" , which is a subclass of "+super.tostring();
	}
	public void setLength(double length)
	{
		this.getLength();
	}
	public void setWidth(double width)
	{
		this.getWidth();
	}
}
