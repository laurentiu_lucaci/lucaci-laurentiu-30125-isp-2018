package g30125.lucaci.laurentiu.l4.e8;

public class Shape {
	String color="red";
	boolean filled= true;
	public Shape()
	{
		color="green";
		filled= true;
	}
	public Shape(String color, boolean filled)
	{
		this.color=color;
		this.filled= filled;
	}
	public String getColor()
	{
		return color;
	}
	public void setColor(String color)
	{
		this.color=color;
	}
	public boolean isFILLED()
	{
		return filled;
	}
	public void setFilled(boolean filled)
	{
		this.filled=filled;
	}
	public String tostring()
	{
		if(isFILLED()==true)
			return "A shape with color of "+getColor()+" and filled";
		else 
			return "A shape with color of "+getColor()+" and Not filled";
	}
}
