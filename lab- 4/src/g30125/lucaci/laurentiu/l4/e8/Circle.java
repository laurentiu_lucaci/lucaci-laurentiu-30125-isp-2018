package g30125.lucaci.laurentiu.l4.e8;

import g30125.lucaci.laurentiu.l4.e8.Shape;

public class Circle extends Shape{
	double radius=1.0;
	public Circle()
	{
		radius=1.0;
	}
	public Circle(double radius)
	{
		this.radius=radius;
	}
	public Circle(double radius, String color, boolean filled)
	{
		this.radius=radius;
		this.color=color;
		this.filled= filled;
	}
	public double getRadius()
	{
		return radius;
	}
	public double getArea()
	{
		return Math.PI*Math.pow(radius, 2);
	}
	public double getPerimeter()
	{
		return 2*Math.PI*radius;
	}
	@Override public String tostring()
	{
		return "A Circle with radius= "+getRadius()+", which is a subclass of "+super.tostring();
	}
}
