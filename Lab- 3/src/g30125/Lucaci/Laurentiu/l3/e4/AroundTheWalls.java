package g30125.Lucaci.Laurentiu.l3.e4;

import becker.robots.*;

public class AroundTheWalls {
	public static void main(String[] args)
	{
		City Cluj=new City();
		Wall block1=new Wall(Cluj,1,1,Direction.WEST);
		Wall block2=new Wall(Cluj,2,1,Direction.WEST);
		Wall block3=new Wall(Cluj,2,1,Direction.SOUTH);
		Wall block4=new Wall(Cluj,2,2,Direction.SOUTH);
		Wall block5=new Wall(Cluj,2,2,Direction.EAST);
		Wall block6=new Wall(Cluj,1,2,Direction.EAST);
		Wall block7=new Wall(Cluj,1,1,Direction.NORTH);
		Wall block8=new Wall(Cluj,1,2,Direction.NORTH);
	    Robot Deedee = new Robot(Cluj, 0, 2, Direction.WEST);
	    for(int i=0;i<2;i++)
		{
	    	Deedee.move();
		}
	    Deedee.turnLeft();
	    for(int i=0;i<3;i++)
		{
	    	Deedee.move();
		}
	    Deedee.turnLeft();
	    for(int i=0;i<3;i++)
		{
	    	Deedee.move();
		}
	    Deedee.turnLeft();
	    for(int i=0;i<3;i++)
		{
	    	Deedee.move();
		}
	    Deedee.turnLeft();
	    Deedee.move();
	}
}
