
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import java.util.*;

public class Main {

    public static int solution(int[] A){

        //throw new NotImplementedException();
        for(int i=0;i<A.length;i++)
        {
        	int k=1;
        	for (int j=0;j<A.length;j++)
        	{
        		if(i!=j)
					if(A[i]==A[j])
						k++;
			}
			if(k%2==1)
			{
				return A[i];
			}
		}
		return 0;
        }

    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
        int result = solution(A);

        if(result==7)
            System.out.println("Rezultat corect.");
        else
            System.out.println("Rezultat incorect.");
    }
}