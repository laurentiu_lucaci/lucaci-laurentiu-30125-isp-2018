package g30125.Lucaci.Laurentiu.l3.e6;

 class MyPoint {
	private int x,y;
	MyPoint()
	{
		x=0;
		y=0;
	}
	MyPoint(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	int getX()
	{
		return x;
	}
	int getY()
	{
		return y;
	}
	void setX(int x)
	{
		this.x=x;
	}
	void setY(int y)
	{
		this.y=y;
	}
	void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	String ToString()
	{
		return "("+this.x+","+this.y+")";
	}
	double distance(int x,int y)
	{
		return Math.sqrt(Math.pow(x-this.x, 2)+Math.pow(y-this.y, 2));
	}
	double distance(MyPoint another)
	{
		return Math.sqrt(Math.pow(another.getX()-this.x, 2)+Math.pow(another.getY()-this.y, 2));
	}
}
