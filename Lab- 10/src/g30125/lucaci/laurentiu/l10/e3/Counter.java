package g30125.lucaci.laurentiu.l10.e3;

	public class Counter extends Thread {

	    static int k = 1;
	    private Thread t;
	    Counter(String name, Thread t){
	        super(name);
	        this.t=t;
	    }

	    public void run(){

	            try {
	                if (t!=null) t.join();
	                System.out.println(getName() + " has started.");
	                for(int i=0;i<100;i++) {
	                    System.out.println("" + k);
	                    k++;
	                    Thread.sleep(1000);
	                }
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }

	        System.out.println(getName() + " has finished.\n");
	    }

	    public static void main(String[] args) {
	        Counter c1 = new Counter("counter1",null);
	        Counter c2 = new Counter("counter2",c1);

	        c1.run();
	        c2.run();
	    }
	}