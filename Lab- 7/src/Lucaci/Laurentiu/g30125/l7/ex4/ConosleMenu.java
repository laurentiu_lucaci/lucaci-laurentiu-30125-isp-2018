package lucaci.laurentiu.g30125.l7.ex4;

import java.util.Scanner;

public class ConosleMenu {

    public static void main(String[] args) {

        Dictionary d = new Dictionary();
        int option = 1; //Option is set on 1, so the menu can be displayed at least once.
        String word;
        String definition;

        while (option!=0){
            displayMenu();
            Scanner in = new Scanner(System.in);
            option = in.nextInt();

            switch (option) {
                case 1: {           //add word
                    System.out.println("Introduceti cuvantul: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    System.out.println("Introduceti definitia: ");
                    Scanner inDef = new Scanner(System.in);
                    definition = inDef.next();
                    d.addWord(new Word(word),new Definition(definition));
                    break;
                }
                case 2: {           //displays the definition for a given word
                    System.out.println("Introduceti cuvantul: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    d.getDefinition(new Word(word));
                    break;
                }
                case 3: {
                    d.getAllWords();
                    break;
                }
                case 4: {
                    d.getAllDefinitions();
                    break;
                }
                case 0: {
                    break;
                }
            }
        }
    }

    private static void displayMenu() {
        System.out.println("Meniu:");
        System.out.println("1. Adaugare cuvant");
        System.out.println("2. Definitia");
        System.out.println("3. Toate cuvintele");
        System.out.println("4. Toate definitiile");
        System.out.println("0. Iesire");
        System.out.println("Optiune: ");
    }
}
