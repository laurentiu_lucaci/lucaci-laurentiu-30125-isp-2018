package lucaci.laurentiu.g30125.l7.ex4;

public class Definition {

    private String description;
    //private Word word;

    public Definition(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
