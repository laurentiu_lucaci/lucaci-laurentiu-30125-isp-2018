package lucaci.laurentiu.g30125.l7.ex3;


import java.util.Comparator;
import java.util.TreeSet;

public class Bank {

    TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(new balanceComp());

    public void addAccount(String owner, double balance){
        BankAccount b = new BankAccount(owner,balance);
        accounts.add(b);
    }

    public void printAccounts(){
        for (BankAccount b : accounts){
            System.out.println(b.getOwner() + " " + b.getBalance());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        for (BankAccount b : accounts){
            if (b.getBalance()>minBalance && b.getBalance()<maxBalance){
                System.out.println(b.getOwner() + " " + b.getBalance());
            }
        }
    }

    public BankAccount getAccount (String owner) {
        for (BankAccount b : accounts){
            if (b.getOwner().equals(owner))
                return b;
        }
        return null;
    }

    public TreeSet<BankAccount> getAllAccounts(){
        return accounts;
    }
}

class balanceComp implements Comparator<BankAccount>{

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return Double.compare(o1.getBalance(),o2.getBalance());
    }
}
