package lucaci.laurentiu.g30125.l7.ex2;


import java.util.ArrayList;
import java.util.Collections;

public class Bank {

    ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

    public Bank() {
    }

    public void addAccount(String owner, double balance){
        BankAccount b = new BankAccount(owner,balance);
        accounts.add(b);
    }

    public void printAccounts(){
        Collections.sort(accounts);
        for (BankAccount b : accounts){
            System.out.println(b.getOwner() + " " + b.getBalance());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        Collections.sort(accounts);
        for (BankAccount b : accounts){
            if (b.getBalance() > minBalance && b.getBalance() < maxBalance) {
                System.out.println(b.getOwner() + " " + b.getBalance());
            }
        }
    }

    public BankAccount getAccount (String owner) {
        for (BankAccount b : accounts){
            if (b.getOwner().equals(owner))
                return b;
        }
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts(){
        return accounts;
    }
}
