package lucaci.laurentiu.g30125.l7.ex1;


import java.util.Comparator;

public class BankAccount implements Comparable {

        private String owner;
        private double balance;

        public BankAccount(String owner) {
            this.owner =  owner;
        }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount ) {
            if (balance > amount ) {
                balance -= amount;
            } else
                System.out.println("You don't have enough money in your account.");
        }

        public void deposit(double amount) {
            balance+=amount;
            System.out.println("Amount added.");
        }

        @Override
        public boolean equals(Object obj) {
            BankAccount o = (BankAccount) obj;
            if (o!= null && this.owner.equals(o.getOwner()) && this.balance==o.balance){
                return true;
            } else
                return false;
        }

        @Override
        public int hashCode() {
            int result = 13;
            result = result * owner.hashCode() * (int) balance;
            return result;
        }

        public String getOwner() {
            return owner;
        }

        public double getBalance() {
            return balance;
        }



    @Override
    public int compareTo(Object b) {
        double compareBalance = ((BankAccount) b).getBalance();
        return Double.compare(this.getBalance(), compareBalance);
    }

    public static void main(String[] args) {

        BankAccount b1 = new BankAccount("Victor");
        BankAccount b2 = new BankAccount("David");

        b1.deposit(523);
        b1.deposit(274.3);
        b2.deposit(912);
        b2.withdraw(500);

        System.out.println(b1.getBalance());
        System.out.println(b2.getBalance());

        b2.withdraw(1000);

        BankAccount b3 = b1;

        if (b1.equals(b2)){
            System.out.println("Equal");}
        else
            System.out.println("Not equal");


        if (b1.equals(b3)){
            System.out.println("Equal");}
        else
            System.out.println("Not equal");

        System.out.println("b1 " + b1.hashCode());
        System.out.println("b2 " + b2.hashCode());
    }
}
