package ex1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;

    public Shape(Color color, int x, int y, String id,boolean fill) {
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public abstract void draw(Graphics g);
}
