package ex1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color,int x, int y,String id, int length,boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+this.length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if (getFill()){
            g.fillRect(getX(),getY(),length,length);
        }
    }
}
