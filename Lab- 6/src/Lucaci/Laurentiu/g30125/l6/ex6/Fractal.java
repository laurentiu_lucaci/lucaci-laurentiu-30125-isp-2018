package ex6;

import javax.swing.*;
import java.awt.*;

public class Fractal extends JFrame {

    private int x;
    private int y;
    private int length;

    public Fractal(int x, int y, int length) {
        this.x = x;
        this.y = y;
        this.length = length;
    }

    public void paint(Graphics g){
        while(length>20){
            g.setColor(Color.ORANGE);
            g.fillOval(x,y,length,length);
            g.setColor(Color.CYAN);
            g.fillRect(x+10,y+10,length-20,length-20);
            x=(int)(x+(length-(Math.sqrt(length*length/2)))/2);
            y=(int)(y+(length-(Math.sqrt(length*length/2)))/2);
            length=(int)(Math.sqrt(length*length/2));
        }
    }


    public static void main(String[] args) {

        Fractal element = new Fractal(300,300,500);
        element.setTitle("Fractal");
        element.setSize(1000,1000);
        element.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        element.setVisible(true);
    }
}
