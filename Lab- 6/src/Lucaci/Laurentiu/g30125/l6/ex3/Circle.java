package ex3;

import java.awt.*;

public class Circle implements Shape{
    private Color color;
    private int x;
    private int y;
    private String id;
    private Boolean fill;
    private int radius;
    Graphics g;

    public Circle(Color color, int x, int y, String id, Boolean fill, int radius) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
        this.radius = radius;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if (fill){
            g.fillOval(x,y,radius,radius);
        }
    }

}
