package ex3;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Circle s1 = new Circle(Color.DARK_GRAY,50,60, "Cercul_1", true, 90);
        b1.addShape(s1);
        Circle s2 = new Circle(Color.YELLOW,200,180,"Cercul_2", true,100);
        b1.addShape(s2);
        Rectangle s3 = new Rectangle(Color.BLUE,100,160,"Rectangle_1", false,50);
        b1.addShape(s3);
        b1.deleteByID("Cercul_1");
    }

}
