package ex5;

public class Brick {

    private int height;
    private int width;

    public Brick(int height, int width) {
        this.height = height;
        this.width = width;
    }


    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }


}
