package g30125.lucaci.laurentiu.l8.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class search {
    public static void main(String[] args){
        int count=0;
        try
        {
            File f=new File("C:\\Users\\Sebi\\Desktop\\Data.txt");
            String c="ala";
            Scanner cin = new Scanner(f);
            while(cin.hasNext())
            {
                if(cin.next().equals(c))
                    count++;
            }
            System.out.println("Caracterul '"+c+"' apare de "+count + " ori");
            cin.close();
        }catch (FileNotFoundException e){
            System.out.println("Nu s-a gasit fisierul");
        }
    }
}
