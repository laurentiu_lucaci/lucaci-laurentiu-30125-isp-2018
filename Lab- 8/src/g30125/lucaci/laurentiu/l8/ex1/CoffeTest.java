package g30125.lucaci.laurentiu.l8.ex1;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
            Cofee c = mk.makeCofee();
            try {
                d.drinkCofee(c);
            } catch (NumberException e) {
                System.out.println("Exception:" +e.getMessage()+" nr: "+e.getNb());
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}//.class

class CofeeMaker {
    int nr=0;
    Cofee makeCofee(){
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        nr++;
        Cofee cofee = new Cofee(t,c,nr);
        return cofee;
    }

}//.class

class Cofee{
    private int temp;
    private int conc;
    private int number;

    Cofee(int t,int c,int nr){temp = t;conc = c;number=nr;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getNumber(){return number;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+" nr: "+number+" ]";}
}//.class

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, NumberException{
        if (c.getNumber()> 10) {
            throw new NumberException(c.getNumber(),"Too manny cofee!");
        }
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");

        System.out.println("Drink cofee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}//.class

class NumberException extends Exception{
    int nr;

    public NumberException(int nr,String message) {
        super(message);
        this.nr = nr;
    }

    public int getNb() {
        return nr;
    }
}