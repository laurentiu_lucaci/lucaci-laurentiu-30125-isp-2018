package g30125.Lucaci.Laurentiu.l2.e6;

import java.util.*;

public class ex6 {
	public static int Recursiv(int n)
	{
		if(n==1||n==0) return 1;
		else return n*Recursiv(n-1);
	}
	
	public static int Nerecursiv(int n)
	{
		int rez=1;
		if(n==0)
			return 1;
		for(int i=1;i<=n;i++)
			rez=rez*i;
		return rez;
	}
	
	public static void main(String[] args)
	{
		Scanner Cin=new Scanner(System.in);
		int N;
		N=Cin.nextInt();
		int rez1=1,rez2;
		rez1=Nerecursiv(N);
		System.out.println("Rezultatul nerecursiv: "+rez1);
		rez2=Recursiv(N);
		System.out.println("Rezultatul recursiv: " + rez2);	
	}
}
