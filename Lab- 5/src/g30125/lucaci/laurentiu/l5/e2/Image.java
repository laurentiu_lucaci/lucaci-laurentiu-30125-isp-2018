package g30125.lucaci.laurentiu.l5.e2;

public interface Image {
	   void display();
	}
	 
	class RealImage implements Image {
	 
	   private String fileName;
	 
	   public RealImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	 
	   @Override
	   public void display() {
	      System.out.println("Displaying " + fileName);
	   }
	 
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	
	
	public class ProxyImage implements Image{

        private RealImage realImage;
        private RotatedImage rotatedImage;
        private String fileName;

        public ProxyImage(String fileName){
            this.fileName = fileName;
        }

        @Override
        public void display() {
            if (fileName.endsWith("png"))
            {
                realImage = new RealImage(fileName);
                realImage.display();
            }
            else {
                rotatedImage = new RotatedImage(fileName);
                rotatedImage.display();
            }
        }
}
	public class RotatedImage implements Image {
		 
		   private String fileName;

		   public RotatedImage(String fileName){
		      this.fileName = fileName;
		      loadFromDisk(fileName);
		   }
		   @Override
		   public void display() {
		      System.out.println("Display rotated " + fileName);
		   }
	}
	}
	