package g30125.lucaci.laurentiu.l5.e2;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;


public class TestImage {
	@Test
	public void shouldDisplay()
	{
		RealImage r1=new RealImage("CaiVerzi.jpg");
		assertEquals("Displaying CaiVerzi.jpg", r1.display());
	}
}
