package g30125.lucaci.laurentiu.l5.e1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class TestShape {
	@Test
	public void shouldCircleGetArea()
	{
		Circle c1=new Circle(3);
		assertEquals(28.274338823,c1.getArea(),001);
	}
	@Test
	public void shouldCircleGetPerimeter()
	{
		Circle c1=new Circle(3);
		assertEquals(18.84955,c1.getPerimeter(),001);
	}
	@Test
	public void shouldRectangleGetArea()
	{
		Rectangle r1=new Rectangle(4,6);
		assertEquals(24,r1.getArea(),001);
	}
	@Test
	public void shouldRectangleGetPerimeter()
	{
		Rectangle r1=new Rectangle(4,6);
		assertEquals(20,r1.getPerimeter(),001);
	}
}
