package g30125.lucaci.laurentiu.l9.ex3;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileContent extends JFrame {

    JButton button;
    JLabel file;
    JTextField textField, tFile;

    FileContent(){
        setTitle("File Display");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        file = new JLabel("File Name:");
        file.setBounds(10,50,100,30);
        file.setBackground(Color.white);

        tFile = new JTextField();
        tFile.setBounds(120,50,200,30);

        button = new JButton("Search file");
        button.setBounds(10,100,100,30);
        button.setBackground(Color.cyan);

        textField = new JTextField();
        textField.setBounds(10,150,300,300);

        button.addActionListener(new checkIfPressed());

        add(file);
        add(tFile);
        add(button);
        add(textField);

    }

    public static void main(String[] args) {
        new FileContent();
    }

    class checkIfPressed implements ActionListener {

        
        public void actionPerformed(ActionEvent e) {
            BufferedReader br;
            String fileName = FileContent.this.tFile.getText();
            String fileContent="";
            try {
                Path path = Paths.get(fileName);
                if (Files.exists(path)){
                    br = new BufferedReader(new FileReader(fileName));
                    String line = br.readLine();
                    while (line != null){
                        fileContent=fileContent+"\n"+line;
                        line=br.readLine();
                    }
                    FileContent.this.textField.setText(fileContent);
                } else{
                    FileContent.this.textField.setText("File cannot be found.");
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}