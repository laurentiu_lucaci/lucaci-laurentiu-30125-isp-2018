package g30125.lucaci.laurentiu.l9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button extends JFrame  implements ActionListener {
    JButton bClick;
    JLabel lblCount;
    JTextField tfCount;
    int count=0;

    Button(){
        setTitle("Button counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,400);
        init();
        setVisible(true);
    }


    public void init(){
        int width=80,height=30;
        this.setLayout(null);
        lblCount=new JLabel("Count:");
        add(lblCount);
        lblCount.setBounds(60,100,width,height);
        tfCount=new JTextField(count+"",2);
        tfCount.setBounds(120,100,width,height);
        tfCount.setEditable(false);
        add(tfCount);
        bClick = new JButton("Click");
        bClick.setBounds(150,180,width, height);
        add(bClick);
        bClick.addActionListener(this);
    }

    public void actionPerformed(ActionEvent evt) {
        ++count;
        tfCount.setText(count + "");
    }
    public static void main(String[] args) {
        Button a= new Button();
    }
}

